var express = require('express');
var router = express.Router();
var auth = require('./auth');

/* GET home page. */
router.get('/', function(req, res, next) {
  var a = false;
  if (auth.passphrase !== undefined)
    a = auth.passphrase == req.cookies.passphrase;
  res.render('index', { authenticated: a });
});

router.post('/login', function(req, res) {
  if (req.body.passphrase === undefined)
    res.json({ error: 'No passphrase' });
  else if (auth.passphrase === undefined)
    res.json({ error: 'No stored passphrase; This is a bug.' });
  else if (auth.passphrase === req.body.passphrase) {
    // We don't encrypt the connection anyway.
    res.cookie('passphrase', req.body.passphrase, { maxAge: 1000*3600*24*30 });
    res.json({ success: 0 });
  } else {
    res.json({ error: 'Wrong passphrase' });
  }
});

module.exports = router;
